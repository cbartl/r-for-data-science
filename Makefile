RMDFILES := $(wildcard *.Rmd)
HTMLFILES := $(patsubst %.Rmd, _site/%.html, $(RMDFILES))
TEMPFOLDERS := $(patsubst %.Rmd, %_files, $(RMDFILES))

$(info ---------------------------------------------------------------)
$(info RMDFILES:    $(RMDFILES))
$(info HTMLFILES:   $(HTMLFILES))
$(info TEMPFOLDERS: $(TEMPFOLDERS))
$(info ---------------------------------------------------------------)

all: html
	Rscript -e "require(rmarkdown); render_site();"

html: $(HTMLFILES)

clean:
	rm -rfv _site
	rm -rfv $(TEMPFOLDERS)
	
deploy:
	./deploy.sh

_site/%.html: %.Rmd
	Rscript -e "require(rmarkdown); render_site('$<');"

.PHONY: all clean html deploy
