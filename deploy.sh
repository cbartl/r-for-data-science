set -e

SRCROOT=/home/chris/src
VENV=$SRCROOT/nikola
NIKOLA_ROOT=$SRCROOT/nikola/nikola.cbartl.com

pushd _site
SRC=$(pwd)
DST=$NIKOLA_ROOT/files/r4ds

rm -rf $DST && true
mkdir -p $DST
cp -r $SRC/* $DST

. $VENV/bin/activate
cd $NIKOLA_ROOT
nikola build
nikola deploy
deactivate

popd

echo 'Successfully deployed to http://www.cbartl.com/r4ds/index.html'
